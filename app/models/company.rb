class Company < ActiveRecord::Base

  attr_accessor :country_name

  belongs_to :country

  has_many :lines  
  
end
