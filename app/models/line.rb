class Line < ActiveRecord::Base

  belongs_to :company

  has_many :fesco_int_tariff
  has_many :othcs
  has_many :dthcs  

end
