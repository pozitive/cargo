class Dthc < ActiveRecord::Base
  belongs_to :line
  has_many :container_dthcs
  has_many :containers, :through => :container_dthcs
  has_many :dthc_ports
  has_many :ports, :through => :dthc_ports
  attr_reader :container_tokens, :port_tokens
  
  def container_tokens=(ids)
    self.container_ids = ids.split(",")
  end
  def port_tokens=(ids)
    self.port_ids = ids.split(",")
  end

  scope :container, lambda { |container_id| { :joins => :containers,
      :conditions => { :containers => { :id => container_id } } } }
  scope :port, lambda { |port_id| { :joins => :ports,
      :conditions => { :ports => { :id => port_id } } } }
  scope :container_and_line, -> (container,line) { Dthc.container(container) & Dthc.line(line) }
  scope :port_and_container_and_line, -> (port, container, line) { Dthc.port(port) & Dthc.container(container) & Dthc.line(line) }
      
  def self.line(line)
    if line
      where(line_id: line)
    else
      scoped
    end
  end
  
end
