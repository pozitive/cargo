class Port < ActiveRecord::Base
  belongs_to :country
  has_many :othc_ports  
  has_many :othcs, :through => :dthc_ports  
  has_many :dthc_ports  
  has_many :dthcs, :through => :dthc_ports  
  has_many :pol_fesco_int_tariffs
  has_many :pod_fesco_int_tariffs
  has_many :tariffs

  def self.tokens(query)
    ports = where("name like ?", "%#{query}%")
    if ports.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      ports
    end
  end

end
