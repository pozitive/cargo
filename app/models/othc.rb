class Othc < ActiveRecord::Base
  belongs_to :line
  has_many :container_othcs
  has_many :containers, :through => :container_othcs
  has_many :othc_ports
  has_many :ports, :through => :othc_ports
  validates :line_id, :price_currency, presence: true
  monetize :price_cents
  attr_reader :container_tokens, :port_tokens

  def container_tokens=(ids)
    self.container_ids = ids.split(",")
  end
  def port_tokens=(ids)
    self.port_ids = ids.split(",")
  end
  def line_name
    line.try(:name)
  end
  
  def line_name=(name)
    self.line = Line.find_by_name(name) if name.present?
  end

  scope :container, lambda { |container_id| { :joins => :containers,
      :conditions => { :containers => { :id => container_id } } } }
  scope :port, lambda { |port_id| { :joins => :ports,
      :conditions => { :ports => { :id => port_id } } } }
  scope :container_and_line, -> (container,line) { Othc.container(container) & Othc.line(line) }
  scope :port_and_container_and_line, -> (port, container, line) { Othc.port(port) & Othc.container(container) & Othc.line(line) }
      
  def self.line(line)
    if line
      where(line_id: line)
    else
      scoped
    end
  end

end
