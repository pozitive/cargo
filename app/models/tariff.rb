class Tariff < ActiveRecord::Base

  belongs_to :container
  belongs_to :pol, class_name: "Port"
  belongs_to :pod, class_name: "Port"
  validates :pol_id, :pod_id, presence: true
  attr_accessor :container_name, :pol_name, :pod_name  
  def container_name
    container.try(:name)
  end
  def container_name=(name)
    self.container = Containers.find_by_name(name) if name.present?
  end    
  def pol_name
    pol.try(:name)
  end
  def pol_name=(name)
    self.pol = Port.find_by_name("Vlavivostok") if name.present?
  end    
  def pod_name
    pod.try(:name)
  end
  def pod_name=(name)
    self.pod = Port.find_by_name(name) if name.present?
  end
  
  
  
  def rates
    @rates = find_rates
  end
 
  def othc

  end
  
private
  
  def find_rates

    rates = FescoIntTariff.all
    rates = rates.pol_and_pod_and_container(pol_id,pod_id,container_id) if container_id.present?
    rates = rates.pol_and_pod(pol_id,pod_id) unless container_id.present?
    # rates = rates.pol_and_pod_and_full_and_coc_and_line(pol_id,pod_id,full,coc,line_id) unless container_id.present?
    # rates = rates.pod(pod_id)
    # rates = rates.line(line_id)
    # rates = rates.full(full)
    # rates = rates.coc(coc)
    # rates = rates.container(container_id) if container_id.present?
    # rates = rates.pol(pol_id) if pol_id.present?
    # rates = rates.pod(pod_id) if pod_id.present?
    # rates = rates.line(line_id) if line_id.present?
    othc = nil
    dthc = nil
    res = []
    i = 0
    rates.each do |rate|
      containers = rate.containers
      othc = []
      dthc = []
      containers.each do |container|
        othc += Othc.port_and_container_and_line(pol, container, rate.line)
        dthc += Dthc.port_and_container_and_line(pod, container, rate.line)
      end
      othc = othc[0]
      dthc = dthc[0]
      othcprice = othc.price_cents if othc.present?
      dthcprice = dthc.price_cents if dthc.present?
      othcprice ||= 0
      dthcprice ||= 0
      total = othcprice + dthcprice + rate.baf_cents + rate.freight_cents
      res += [rate: rate, othc: othc, dthc: dthc, total: total]
    end
      
    return res
  end 
  
end
