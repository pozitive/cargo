class Container < ActiveRecord::Base

  has_many :container_othcs
  has_many :othcs, :through => :container_othcs

  has_many :dthcs
  has_many :fesco_int_tariff

  has_and_belongs_to_many :dthcs
  has_and_belongs_to_many :fesco_int_tariffs
    
end
