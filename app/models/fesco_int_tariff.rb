class FescoIntTariff < ActiveRecord::Base

  attr_accessor :line_name, :container_tokens, :pol_tokens, :pod_tokens
  
  def container_tokens=(ids)
    self.container_ids = ids.split(",")
    logger = ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
    logger.tagged("BCX") { logger.info "Stuff" }
  end

  def pol_tokens=(ids)
    self.pol_ids = ids.split(",")
    logger = ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
    logger.tagged("BCX") { logger.info ids.split(",") }
  end
  
  def pod_tokens=(ids)
    self.pod_ids = ids.split(",")
    logger = ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
    logger.tagged("BCX") { logger.info "Test" }
  end
  
  # Monetize
  monetize :freight_cents
  monetize :baf_cents
  # Line
  belongs_to :line
  # Pol
  has_many :pol_fesco_int_tariffs
  has_many :pols, through: :pol_fesco_int_tariffs, :source => :port
  # Pod
  has_many :pod_fesco_int_tariffs
  has_many :pods, through: :pod_fesco_int_tariffs, :source => :port
  # Containers
  has_and_belongs_to_many :containers

  accepts_nested_attributes_for(:containers)
  accepts_nested_attributes_for(:pols)
  accepts_nested_attributes_for(:pods)

  
  # Scope
  scope :full, ->(full) { where(full: full ) }
  scope :coc, ->(coc) { where(coc: coc) }
  scope :container, lambda { |container_id| { :joins => :containers,
                                         :conditions => { :containers => { :id => container_id } } } }
  scope :pol, lambda { |pol| { :joins => :pols,
                          :conditions => { :ports => { :id => pol } } } }
  scope :pod, lambda { |pod| { :joins => :pods,
                          :conditions => { :ports => { :id => pod } } } }
  scope :pol_and_pod, -> (pol,pod) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) }
  scope :pol_and_pod_and_container, -> (pol,pod,container) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) & FescoIntTariff.container(container)}
  scope :pol_and_pod_and_container_and_full, -> (pol,pod,container,full) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) & FescoIntTariff.container(container) & FescoIntTariff.full(full)}
  scope :pol_and_pod_and_container_and_full_and_coc, -> (pol,pod,container,full,coc) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) & FescoIntTariff.container(container) & FescoIntTariff.full(full) & FescoIntTariff.coc(coc)}
  scope :pol_and_pod_and_container_and_full_and_coc_and_line, -> (pol,pod,container,full,coc,line) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) & FescoIntTariff.container(container) & FescoIntTariff.full(full) & FescoIntTariff.coc(coc) & FescoIntTariff.line(line)}
  scope :pol_and_pod_and_full_and_coc_and_line, -> (pol,pod,full,coc,line) { FescoIntTariff.pol(pol) & FescoIntTariff.pod(pod) & FescoIntTariff.full(full) & FescoIntTariff.coc(coc) & FescoIntTariff.line(line)}

  def self.line(line)
    if line
      where(line_id: line)
    else
      scoped
    end
  end

  
  
end
