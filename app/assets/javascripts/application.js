// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.ui.autocomplete
//= require jquery_ujs
//= require jquery.tokeninput
//= require autocomplete-rails
//= require turbolinks
//= require foundation
//= require_tree .
// Include all twitter's javascripts


$(function(){ $(document).foundation(); });

$(function () {  
  $('#fesco_int_tariff_container_tokens').tokenInput('/containers.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#fesco_int_tariff').data('pre')
  });  
});  
$(function () {  
  $('#fesco_int_tariff_pol_tokens').tokenInput('/ports.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#fesco_int_tariff').data('pre')
  });  
});  
$(function () {  
  $('#fesco_int_tariff_pod_tokens').tokenInput('/ports.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#fesco_int_tariff').data('pre')
  });  
});  


$(function () {  
  $('#othc_container_tokens').tokenInput('/containers.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#othc').data('pre')
  });  
});  
$(function () {  
  $('#othc_port_tokens').tokenInput('/ports.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#othc').data('pre')
  });  
});  

$(function () {  
  $('#dthc_container_tokens').tokenInput('/containers.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#dthc').data('pre')
  });  
});  
$(function () {  
  $('#dthc_port_tokens').tokenInput('/ports.json', { 
    crossDomain: false,
    theme: 'facebook',
    prePopulate: $('#dthc').data('pre')
  });  
});  
