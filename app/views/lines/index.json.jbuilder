json.array!(@lines) do |line|
  json.extract! line, :code, :name, :description, :company_id
  json.url line_url(line, format: :json)
end
