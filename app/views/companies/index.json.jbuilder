json.array!(@companies) do |company|
  json.extract! company, :code, :name, :description, :website, :logo, :country_id
  json.url company_url(company, format: :json)
end
