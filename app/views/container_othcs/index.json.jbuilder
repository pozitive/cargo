json.array!(@container_othcs) do |container_othc|
  json.extract! container_othc, :container_id, :othc_id
  json.url container_othc_url(container_othc, format: :json)
end
