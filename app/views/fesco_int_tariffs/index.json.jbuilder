json.array!(@fesco_int_tariffs) do |fesco_int_tariff|
  json.extract! fesco_int_tariff, :freight_cents, :baf_cents, :currency, :full, :coc, :container_id, :line_id
  json.url fesco_int_tariff_url(fesco_int_tariff, format: :json)
end
