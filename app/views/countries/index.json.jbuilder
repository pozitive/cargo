json.array!(@countries) do |country|
  json.extract! country, :code, :name, :description
  json.url country_url(country, format: :json)
end
