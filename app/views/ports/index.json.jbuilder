json.array!(@ports) do |port|
  json.extract! port, :locode, :name, :subdiv, :status, :date, :coordinate, :description, :country_id
  json.url port_url(port, format: :json)
end
