json.array!(@othc_ports) do |othc_port|
  json.extract! othc_port, :port_id, :othc_id
  json.url othc_port_url(othc_port, format: :json)
end
