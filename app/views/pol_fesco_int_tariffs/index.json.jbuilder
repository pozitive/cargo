json.array!(@pol_fesco_int_tariffs) do |pol_fesco_int_tariff|
  json.extract! pol_fesco_int_tariff, :port_id, :fesco_int_tariff_id
  json.url pol_fesco_int_tariff_url(pol_fesco_int_tariff, format: :json)
end
