json.array!(@container_dthcs) do |container_dthc|
  json.extract! container_dthc, :container_id, :dthc_id
  json.url container_dthc_url(container_dthc, format: :json)
end
