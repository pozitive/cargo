json.array!(@dthc_ports) do |dthc_port|
  json.extract! dthc_port, :port_id, :dthc_id
  json.url dthc_port_url(dthc_port, format: :json)
end
