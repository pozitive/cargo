json.array!(@containers) do |container|
  json.extract! container, :code, :name, :description
  json.url container_url(container, format: :json)
end
