json.array!(@pod_fesco_int_tariffs) do |pod_fesco_int_tariff|
  json.extract! pod_fesco_int_tariff, :port_id, :fesco_int_tariff_id
  json.url pod_fesco_int_tariff_url(pod_fesco_int_tariff, format: :json)
end
