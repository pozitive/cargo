class PodFescoIntTariffsController < ApplicationController
  before_action :set_pod_fesco_int_tariff, only: [:show, :edit, :update, :destroy]

  # GET /pod_fesco_int_tariffs
  # GET /pod_fesco_int_tariffs.json
  def index
    @pod_fesco_int_tariffs = PodFescoIntTariff.all
  end

  # GET /pod_fesco_int_tariffs/1
  # GET /pod_fesco_int_tariffs/1.json
  def show
  end

  # GET /pod_fesco_int_tariffs/new
  def new
    @pod_fesco_int_tariff = PodFescoIntTariff.new
  end

  # GET /pod_fesco_int_tariffs/1/edit
  def edit
  end

  # POST /pod_fesco_int_tariffs
  # POST /pod_fesco_int_tariffs.json
  def create
    @pod_fesco_int_tariff = PodFescoIntTariff.new(pod_fesco_int_tariff_params)

    respond_to do |format|
      if @pod_fesco_int_tariff.save
        format.html { redirect_to @pod_fesco_int_tariff, notice: 'Pod fesco int tariff was successfully created.' }
        format.json { render action: 'show', status: :created, location: @pod_fesco_int_tariff }
      else
        format.html { render action: 'new' }
        format.json { render json: @pod_fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pod_fesco_int_tariffs/1
  # PATCH/PUT /pod_fesco_int_tariffs/1.json
  def update
    respond_to do |format|
      if @pod_fesco_int_tariff.update(pod_fesco_int_tariff_params)
        format.html { redirect_to @pod_fesco_int_tariff, notice: 'Pod fesco int tariff was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pod_fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pod_fesco_int_tariffs/1
  # DELETE /pod_fesco_int_tariffs/1.json
  def destroy
    @pod_fesco_int_tariff.destroy
    respond_to do |format|
      format.html { redirect_to pod_fesco_int_tariffs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pod_fesco_int_tariff
      @pod_fesco_int_tariff = PodFescoIntTariff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pod_fesco_int_tariff_params
      params.require(:pod_fesco_int_tariff).permit(:port_id, :fesco_int_tariff_id)
    end
end
