class ContainerDthcsController < ApplicationController
  before_action :set_container_dthc, only: [:show, :edit, :update, :destroy]

  # GET /container_dthcs
  # GET /container_dthcs.json
  def index
    @container_dthcs = ContainerDthc.all
  end

  # GET /container_dthcs/1
  # GET /container_dthcs/1.json
  def show
  end

  # GET /container_dthcs/new
  def new
    @container_dthc = ContainerDthc.new
  end

  # GET /container_dthcs/1/edit
  def edit
  end

  # POST /container_dthcs
  # POST /container_dthcs.json
  def create
    @container_dthc = ContainerDthc.new(container_dthc_params)

    respond_to do |format|
      if @container_dthc.save
        format.html { redirect_to @container_dthc, notice: 'Container dthc was successfully created.' }
        format.json { render action: 'show', status: :created, location: @container_dthc }
      else
        format.html { render action: 'new' }
        format.json { render json: @container_dthc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /container_dthcs/1
  # PATCH/PUT /container_dthcs/1.json
  def update
    respond_to do |format|
      if @container_dthc.update(container_dthc_params)
        format.html { redirect_to @container_dthc, notice: 'Container dthc was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @container_dthc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /container_dthcs/1
  # DELETE /container_dthcs/1.json
  def destroy
    @container_dthc.destroy
    respond_to do |format|
      format.html { redirect_to container_dthcs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_container_dthc
      @container_dthc = ContainerDthc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def container_dthc_params
      params.require(:container_dthc).permit(:container_id, :dthc_id)
    end
end
