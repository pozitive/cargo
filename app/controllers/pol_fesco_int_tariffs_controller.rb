class PolFescoIntTariffsController < ApplicationController
  before_action :set_pol_fesco_int_tariff, only: [:show, :edit, :update, :destroy]

  # GET /pol_fesco_int_tariffs
  # GET /pol_fesco_int_tariffs.json
  def index
    @pol_fesco_int_tariffs = PolFescoIntTariff.all
  end

  # GET /pol_fesco_int_tariffs/1
  # GET /pol_fesco_int_tariffs/1.json
  def show
  end

  # GET /pol_fesco_int_tariffs/new
  def new
    @pol_fesco_int_tariff = PolFescoIntTariff.new
  end

  # GET /pol_fesco_int_tariffs/1/edit
  def edit
  end

  # POST /pol_fesco_int_tariffs
  # POST /pol_fesco_int_tariffs.json
  def create
    @pol_fesco_int_tariff = PolFescoIntTariff.new(pol_fesco_int_tariff_params)

    respond_to do |format|
      if @pol_fesco_int_tariff.save
        format.html { redirect_to @pol_fesco_int_tariff, notice: 'Pol fesco int tariff was successfully created.' }
        format.json { render action: 'show', status: :created, location: @pol_fesco_int_tariff }
      else
        format.html { render action: 'new' }
        format.json { render json: @pol_fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pol_fesco_int_tariffs/1
  # PATCH/PUT /pol_fesco_int_tariffs/1.json
  def update
    respond_to do |format|
      if @pol_fesco_int_tariff.update(pol_fesco_int_tariff_params)
        format.html { redirect_to @pol_fesco_int_tariff, notice: 'Pol fesco int tariff was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pol_fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pol_fesco_int_tariffs/1
  # DELETE /pol_fesco_int_tariffs/1.json
  def destroy
    @pol_fesco_int_tariff.destroy
    respond_to do |format|
      format.html { redirect_to pol_fesco_int_tariffs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pol_fesco_int_tariff
      @pol_fesco_int_tariff = PolFescoIntTariff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pol_fesco_int_tariff_params
      params.require(:pol_fesco_int_tariff).permit(:port_id, :fesco_int_tariff_id)
    end
end
