class DthcPortsController < ApplicationController
  before_action :set_dthc_port, only: [:show, :edit, :update, :destroy]

  # GET /dthc_ports
  # GET /dthc_ports.json
  def index
    @dthc_ports = DthcPort.all
  end

  # GET /dthc_ports/1
  # GET /dthc_ports/1.json
  def show
  end

  # GET /dthc_ports/new
  def new
    @dthc_port = DthcPort.new
  end

  # GET /dthc_ports/1/edit
  def edit
  end

  # POST /dthc_ports
  # POST /dthc_ports.json
  def create
    @dthc_port = DthcPort.new(dthc_port_params)

    respond_to do |format|
      if @dthc_port.save
        format.html { redirect_to @dthc_port, notice: 'Dthc port was successfully created.' }
        format.json { render action: 'show', status: :created, location: @dthc_port }
      else
        format.html { render action: 'new' }
        format.json { render json: @dthc_port.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dthc_ports/1
  # PATCH/PUT /dthc_ports/1.json
  def update
    respond_to do |format|
      if @dthc_port.update(dthc_port_params)
        format.html { redirect_to @dthc_port, notice: 'Dthc port was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @dthc_port.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dthc_ports/1
  # DELETE /dthc_ports/1.json
  def destroy
    @dthc_port.destroy
    respond_to do |format|
      format.html { redirect_to dthc_ports_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dthc_port
      @dthc_port = DthcPort.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dthc_port_params
      params.require(:dthc_port).permit(:port_id, :dthc_id)
    end
end
