class OthcsController < ApplicationController
  before_action :set_othc, only: [:show, :edit, :update, :destroy]
  autocomplete :line, :name
  
  # GET /othcs
  # GET /othcs.json
  def index
    @othcs = Othc.all
  end

  # GET /othcs/1
  # GET /othcs/1.json
  def show
    @linename = @othc.line.name if @othc.line.present?
  end

  # GET /othcs/new
  def new
    @othc = Othc.new
    @ports = Port.all
    @containers = Container.all
    @lines = Line.all
  end

  # GET /othcs/1/edit
  def edit
    @ports = Port.all
    @containers = Container.all
    @lines = Line.all
    @linename = @othc.line.name if @othc.line.present?
  end

  # POST /othcs
  # POST /othcs.json
  def create
    @othc = Othc.new(othc_params)
    respond_to do |format|
      if @othc.save
        format.html { redirect_to @othc, notice: 'Othc was successfully created.' }
        format.json { render action: 'show', status: :created, location: @othc }
      else
        format.html { render action: 'new' }
        format.json { render json: @othc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /othcs/1
  # PATCH/PUT /othcs/1.json
  def update
    respond_to do |format|
      if @othc.update(othc_params)
        format.html { redirect_to @othc, notice: 'Othc was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @othc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /othcs/1
  # DELETE /othcs/1.json
  def destroy
    @othc.destroy
    respond_to do |format|
      format.html { redirect_to othcs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_othc
      @othc = Othc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def othc_params
      params.require(:othc).permit( :id, :line_id, :price_cents, :price_currency, :container_tokens, :port_tokens,
                                    { container_tokens: []},                                               
                                    { container_ids: []},
                                    { port_tokens: [] },
                                    { port_ids: [] })
    end

end
