class TariffsController < ApplicationController
  before_action :set_tariff, only: [:show, :edit, :update, :destroy]
  autocomplete :pol, :name
  autocomplete :pod, :name  
  autocomplete :port, :name  
  autocomplete :container, :name

  def index
    @tariff = Tariff.new
    @tariffs = Tariff.all
    @pols = Port.order(:name).take(10).map(&:name)
    @pods = Port.order(:name).take(10).map(&:name)  
    @containers = Container.order(:name).map(&:name)
    @companies = Company.all
    @about = Page.find_by_permalink("about")
    @title = @about.name if @about.present?
    @content = @about.content if @about.present?
  end

  def new
    @tariff = Tariff.new
  end
  
  def show
    @container = @tariff.container.name if @tariff.container.present?
    @container = "all container types" unless @tariff.container.present?
    @companies = Company.all
    @rates = @tariff.rates 
  end

  def edit
    @container = @tariff.container.name if @tariff.container.present?
    @pol = @tariff.pol.name if @tariff.pol.present?
    @pod = @tariff.pod.name if @tariff.pod.present?
  end

  def create
    @tariff = Tariff.new(tariff_params)
    respond_to do |format|
      if @tariff.save
        format.html { redirect_to @tariff, notice: 'Tariff was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @tariff.update(tariff_params)
        format.html { redirect_to @tariff, notice: 'Tariff was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tariff.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
  
    def set_tariff
      @tariff = Tariff.find(params[:id])
    end
  
    def tariff_params
      params.require(:tariff).permit( :id, :pol_id, :pod_id, :container_id, :full, :coc, :currency, :line_id, :min_price, :max_price )
    end
    
end
