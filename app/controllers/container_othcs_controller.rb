class ContainerOthcsController < ApplicationController
  before_action :set_container_othc, only: [:show, :edit, :update, :destroy]

  # GET /container_othcs
  # GET /container_othcs.json
  def index
    @container_othcs = ContainerOthc.all
  end

  # GET /container_othcs/1
  # GET /container_othcs/1.json
  def show
  end

  # GET /container_othcs/new
  def new
    @container_othc = ContainerOthc.new
  end

  # GET /container_othcs/1/edit
  def edit
  end

  # POST /container_othcs
  # POST /container_othcs.json
  def create
    @container_othc = ContainerOthc.new(container_othc_params)

    respond_to do |format|
      if @container_othc.save
        format.html { redirect_to @container_othc, notice: 'Container othc was successfully created.' }
        format.json { render action: 'show', status: :created, location: @container_othc }
      else
        format.html { render action: 'new' }
        format.json { render json: @container_othc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /container_othcs/1
  # PATCH/PUT /container_othcs/1.json
  def update
    respond_to do |format|
      if @container_othc.update(container_othc_params)
        format.html { redirect_to @container_othc, notice: 'Container othc was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @container_othc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /container_othcs/1
  # DELETE /container_othcs/1.json
  def destroy
    @container_othc.destroy
    respond_to do |format|
      format.html { redirect_to container_othcs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_container_othc
      @container_othc = ContainerOthc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def container_othc_params
      params.require(:container_othc).permit(:container_id, :othc_id)
    end
end
