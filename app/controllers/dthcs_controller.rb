class DthcsController < ApplicationController
  before_action :set_dthc, only: [:show, :edit, :update, :destroy]
  autocomplete :line, :name
  
  # GET /dthcs
  # GET /dthcs.json
  def index
    @dthcs = Dthc.all
  end

  # GET /dthcs/1
  # GET /dthcs/1.json
  def show
    @linename = @dthc.line.name if @dthc.line.present?    
  end

  # GET /dthcs/new
  def new
    @dthc = Dthc.new
    @ports = Port.all
    @containers = Container.all
    @lines = Line.all
  end

  # GET /dthcs/1/edit
  def edit
    @ports = Port.all
    @containers = Container.all
    @lines = Line.all
    @linename = @dthc.line.name if @dthc.line.present?
  end

  # POST /dthcs
  # POST /dthcs.json
  def create
    @dthc = Dthc.new(dthc_params)

    respond_to do |format|
      if @dthc.save
        format.html { redirect_to @dthc, notice: 'Dthc was successfully created.' }
        format.json { render action: 'show', status: :created, location: @dthc }
      else
        format.html { render action: 'new' }
        format.json { render json: @dthc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dthcs/1
  # PATCH/PUT /dthcs/1.json
  def update
    respond_to do |format|
      if @dthc.update(dthc_params)
        format.html { redirect_to @dthc, notice: 'Dthc was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @dthc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dthcs/1
  # DELETE /dthcs/1.json
  def destroy
    @dthc.destroy
    respond_to do |format|
      format.html { redirect_to dthcs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dthc
      @dthc = Dthc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dthc_params
      params.require(:dthc).permit(:port_id, :line_id, :container_id, :price_cents, :price_currency, :container_tokens, :port_tokens,
                                    { container_tokens: []},                                               
                                    { container_ids: []},
                                    { port_tokens: [] },
                                    { port_ids: [] })
    end
end
