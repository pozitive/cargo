class OthcPortsController < ApplicationController
  before_action :set_othc_port, only: [:show, :edit, :update, :destroy]

  # GET /othc_ports
  # GET /othc_ports.json
  def index
    @othc_ports = OthcPort.all
  end

  # GET /othc_ports/1
  # GET /othc_ports/1.json
  def show
  end

  # GET /othc_ports/new
  def new
    @othc_port = OthcPort.new
  end

  # GET /othc_ports/1/edit
  def edit
  end

  # POST /othc_ports
  # POST /othc_ports.json
  def create
    @othc_port = OthcPort.new(othc_port_params)

    respond_to do |format|
      if @othc_port.save
        format.html { redirect_to @othc_port, notice: 'Othc port was successfully created.' }
        format.json { render action: 'show', status: :created, location: @othc_port }
      else
        format.html { render action: 'new' }
        format.json { render json: @othc_port.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /othc_ports/1
  # PATCH/PUT /othc_ports/1.json
  def update
    respond_to do |format|
      if @othc_port.update(othc_port_params)
        format.html { redirect_to @othc_port, notice: 'Othc port was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @othc_port.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /othc_ports/1
  # DELETE /othc_ports/1.json
  def destroy
    @othc_port.destroy
    respond_to do |format|
      format.html { redirect_to othc_ports_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_othc_port
      @othc_port = OthcPort.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def othc_port_params
      params.require(:othc_port).permit(:port_id, :othc_id)
    end
end
