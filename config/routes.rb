Cargo::Application.routes.draw do
  resources :dthc_ports

  resources :container_dthcs

  resources :pages

  resources :othc_ports

  resources :container_othcs

  devise_for :users
#  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  
  resources :pod_fesco_int_tariffs

  resources :pol_fesco_int_tariffs

  get "welcome/index"
  resources :othcs do
    get :autocomplete_line_name, :on => :collection
  end
  resources :dthcs do
    get :autocomplete_line_name, :on => :collection
  end


  resources :fesco_int_tariffs do
    get :autocomplete_line_name, :on => :collection
    get :autocomplete_container_name, :on => :collection    
  end

  # resources :lines
  resources :lines do
    get :autocomplete_line_name, :on => :collection
  end

  resources :containers

  resources :ports

  resources :companies do
    get :autocomplete_country_name, :on => :collection
  end
    
  resources :countries
  
  resources :tariffs do
    get :autocomplete_port_name, :on => :collection    
    get :autocomplete_pol_name, :on => :collection    
    get :autocomplete_pod_name, :on => :collection    
    get :autocomplete_container_name, :on => :collection    
  end
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root "tariffs#index"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
