#! /usr/bin/env ruby

require 'csv'

def fesco_int_tariff_csv (directory = "")
  Dir.glob(directory + "*.csv") do |item|
    csv_text = File.read(item)
    CSV.parse(csv_text, :headers => true) do |row|
      row.to_hash!
      puts row
    end
  end
end

fesco_int_tariff_csv
