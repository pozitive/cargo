# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131217044538) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.text     "description"
    t.string   "website"
    t.string   "logo"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "container_dthcs", force: true do |t|
    t.integer  "container_id"
    t.integer  "dthc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "container_othcs", force: true do |t|
    t.integer  "container_id"
    t.integer  "othc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "containers", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "containers_fesco_int_tariffs", id: false, force: true do |t|
    t.integer "container_id"
    t.integer "fesco_int_tariff_id"
  end

  create_table "countries", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dthc_ports", force: true do |t|
    t.integer  "port_id"
    t.integer  "dthc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dthcs", force: true do |t|
    t.integer  "line_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "full"
    t.integer  "coc"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
  end

  create_table "fesco_int_tariffs", force: true do |t|
    t.decimal  "freight_cents"
    t.decimal  "baf_cents"
    t.string   "currency"
    t.boolean  "full"
    t.boolean  "coc"
    t.integer  "line_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lines", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.text     "description"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "othc_ports", force: true do |t|
    t.integer  "port_id"
    t.integer  "othc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "othcs", force: true do |t|
    t.integer  "line_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "full"
    t.integer  "coc"
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
  end

  create_table "pages", force: true do |t|
    t.string   "name"
    t.string   "permalink"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["permalink"], name: "index_pages_on_permalink", using: :btree

  create_table "pod_fesco_int_tariffs", force: true do |t|
    t.integer  "port_id"
    t.integer  "fesco_int_tariff_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pol_fesco_int_tariffs", force: true do |t|
    t.integer  "port_id"
    t.integer  "fesco_int_tariff_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ports", force: true do |t|
    t.string   "locode"
    t.string   "name"
    t.string   "subdiv"
    t.string   "status"
    t.string   "date"
    t.string   "coordinate"
    t.text     "description"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rails_admin_histories", force: true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      limit: 2
    t.integer  "year",       limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], name: "index_rails_admin_histories", using: :btree

  create_table "tariffs", force: true do |t|
    t.integer  "pol_id"
    t.integer  "pod_id"
    t.integer  "container_id"
    t.integer  "line_id"
    t.string   "currency"
    t.decimal  "min_price"
    t.decimal  "max_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "full",         default: true
    t.boolean  "coc",          default: true
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
