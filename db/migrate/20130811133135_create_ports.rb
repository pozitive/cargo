class CreatePorts < ActiveRecord::Migration
  def change
    create_table :ports do |t|
      t.string :locode
      t.string :name
      t.string :subdiv
      t.string :status
      t.string :date
      t.string :coordinate
      t.text :description

      t.references :country
      
      t.timestamps
    end
  end
end
