class CreateOthcsPorts < ActiveRecord::Migration
  def change
    create_table :othcs_ports, :id => false do |t|
      t.integer :othc_id
      t.integer :port_id
    end
  end
end
