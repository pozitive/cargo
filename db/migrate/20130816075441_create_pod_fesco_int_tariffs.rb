class CreatePodFescoIntTariffs < ActiveRecord::Migration
  def change
    create_table :pod_fesco_int_tariffs do |t|
      t.references :port
      t.references :fesco_int_tariff

      t.timestamps
    end
  end
end
