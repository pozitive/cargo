class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :code
      t.string :name
      t.text :description
      t.string :website
      t.string :logo
      t.references :country

      t.timestamps
    end
  end
end
