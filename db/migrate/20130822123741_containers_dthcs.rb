class ContainersDthcs < ActiveRecord::Migration
  def change
    create_table :containers_dthcs, :id => false do |t|
      t.integer :container_id
      t.integer :dthc_id
    end
  end
end
