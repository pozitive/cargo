class AddDetailsToDthc < ActiveRecord::Migration
  def change
    add_column :dthcs, :full, :boolean
    add_column :dthcs, :coc, :integer
  end
end
