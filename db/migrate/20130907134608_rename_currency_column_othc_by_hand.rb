class RenameCurrencyColumnOthcByHand < ActiveRecord::Migration
  def change
    remove_column :othcs, :price_cents, :decimal, :default => true   

    change_table :othcs do |t|
      t.money :price
    end
  end
end
