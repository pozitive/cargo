class CreateDthcsPorts < ActiveRecord::Migration
  def change
    create_table :dthcs_ports, :id => false do |t|
      t.integer :dthc_id
      t.integer :port_id
    end
  end
end
