class CreateDthcs < ActiveRecord::Migration
  def change
    create_table :dthcs do |t|
      t.decimal :price_cents
      t.string :currency

      t.references :line

      t.timestamps
    end
  end
end
