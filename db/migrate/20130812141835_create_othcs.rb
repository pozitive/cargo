class CreateOthcs < ActiveRecord::Migration
  def change
    create_table :othcs do |t|
      t.decimal :price_cents
      t.string :currency

      t.references :line

      t.timestamps
    end
  end
end
