class DropContainersDthcs < ActiveRecord::Migration
  def up
    drop_table :containers_dthcs
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
