class CreateFescoIntTariffs < ActiveRecord::Migration
  def change
    create_table :fesco_int_tariffs do |t|
      t.decimal :freight_cents
      t.decimal :baf_cents
      t.string :currency
      t.boolean :full
      t.boolean :coc

      t.references :container
      t.references :line

      t.timestamps
    end
  end
end
