class CreateContainerDthcs < ActiveRecord::Migration
  def change
    create_table :container_dthcs do |t|
      t.integer :container_id
      t.integer :dthc_id

      t.timestamps
    end
  end
end
