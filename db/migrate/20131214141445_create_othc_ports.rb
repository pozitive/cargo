class CreateOthcPorts < ActiveRecord::Migration
  def change
    create_table :othc_ports do |t|
      t.integer :port_id
      t.integer :othc_id

      t.timestamps
    end
  end
end
