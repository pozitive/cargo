class ChangeFormatIntToTariffs < ActiveRecord::Migration
  def change
    remove_column :tariffs, :full, :boolean, :default => true
    remove_column :tariffs, :coc, :boolean, :default => true   
    add_column :tariffs, :full, :boolean, :default => true
    add_column :tariffs, :coc, :boolean, :default => true   
  end
end
