class CreateDthcPorts < ActiveRecord::Migration
  def change
    create_table :dthc_ports do |t|
      t.integer :port_id
      t.integer :dthc_id

      t.timestamps
    end
  end
end
