class DropOthcsPorts < ActiveRecord::Migration
  def up
    drop_table :othcs_ports
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
