class CreateContainersFescoIntTariffs < ActiveRecord::Migration
  def change
    create_table :containers_fesco_int_tariffs, :id => false do |t|
      t.integer :container_id
      t.integer :fesco_int_tariff_id
    end
  end
end
