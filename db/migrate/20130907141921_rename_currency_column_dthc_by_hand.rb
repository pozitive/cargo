class RenameCurrencyColumnDthcByHand < ActiveRecord::Migration
  def change
    remove_column :othcs, :currency, :string, :default => true   
    remove_column :dthcs, :currency, :string, :default => true   
    remove_column :dthcs, :price_cents, :string, :default => true   

    change_table :dthcs do |t|
      t.money :price
    end
  end
end
