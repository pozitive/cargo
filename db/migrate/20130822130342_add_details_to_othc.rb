class AddDetailsToOthc < ActiveRecord::Migration
  def change
    add_column :othcs, :full, :boolean
    add_column :othcs, :coc, :integer
  end
end
