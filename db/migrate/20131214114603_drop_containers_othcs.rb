class DropContainersOthcs < ActiveRecord::Migration
  def up
    drop_table :containers_othcs
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
