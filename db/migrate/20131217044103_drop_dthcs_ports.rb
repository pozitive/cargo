class DropDthcsPorts < ActiveRecord::Migration
  def up
    drop_table :dthcs_ports
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
