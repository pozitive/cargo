class CreateTariffs < ActiveRecord::Migration
  def change
    create_table :tariffs do |t|
      t.integer :pol_id
      t.integer :pod_id
      t.references :container
      t.references :line
      t.integer :full
      t.integer :coc
      t.string :currency
      t.decimal :min_price
      t.decimal :max_price

      t.timestamps
    end
  end
end
