class CreateContainerOthcs < ActiveRecord::Migration
  def change
    create_table :container_othcs do |t|
      t.integer :container_id
      t.integer :othc_id

      t.timestamps
    end
  end
end
