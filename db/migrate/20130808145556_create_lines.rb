class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.string :code
      t.string :name
      t.text :description
      t.references :company

      t.timestamps
    end
  end
end
