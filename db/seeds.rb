# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'

# # Add country
# def countries_csv (directory = "db/seed/countries/")
#   Dir.glob(directory + "*.csv") do |item|
#     csv_text = File.read(item)
#     CSV.parse(csv_text, :headers => true) do |row|
#       x = row.to_hash
#       y = { "description" => ''}
#       Country.find_or_create_by_name(x.merge!(y))
#     end
#   end
# end
# # Country.delete_all
# countries_csv




# # Add companies
# # Company.delete_all
# companies = [
#   ['Far Eastern Shipping Company', 'FESCO', "The Far-Eastern Shipping Company (FESCO; Russian: Дальневосточное морское пароходство, tr. Dal'nevostochnoye morskoye parokhodstvo) is a Russian ship-owning company and the base company of FESCO Group. Founded in the 19th century in Vladivostok, the company now owns a fleet of 50 vessels, totaling 880 thousand DWT (Dead Weight Tons)", 'fesco.ru', 'companies/fesco_logo.gif', Country.find_by_code('RU').id],
#   ['Sakhalin Shipping Company', 'SASCO', "Sakhalin Shipping Company (SASCO) (Russian: Сахалинское морское пароходство) is a Russian shipping company. Headquartered in Kholmsk, on Sakhalin Island's west coast, the company was created in 1945, and privatized in 1992.", 'sasco.ru', 'companies/sasco_logo.png', Country.find_by_code('RU').id],
#   ['MCC Transport', 'MCC', "MCC Transport is a regional specialist handling all Intra–Asia containerized cargo for the A.P. Moller – Maersk Group in addition to providing feeder services for a wide range of regional and global shipping lines. Having operated in the market for more than 20 years, MCC Transport represents a group of service oriented and knowledgeable people providing customers with a wide-range direct port coverage and a large corridor portfolio in the Intra Asian market.", 'mcc.com.sg', 'companies/mcc_logo.png', Country.find_by_code('SG').id],
#   ['Maersk Group', 'MAERSK', "Maersk is the best company", 'www.maersk.com', 'companies/maersk_logo.png', Country.find_by_code('DK').id],
#   ['CMA CGM Group', 'CMA-CGM', "CMA CGM: the world’s third largest container shipping group. Founded in 1978 by Jacques R. Saadé, today CMA CGM is the world’s third largest container shipping group and number one in France.", 'www.cma-cgm.com', 'companies/cmacgm_logo.png', Country.find_by_code('FR').id]
# ]
# companies.each do |company|
#   Company.find_or_create_by_name(
#     :name        => company[0],
#     :code        => company[1],
#     :description => company[2],
#     :website     => company[3],
#     :logo        => company[4],
#     :country_id  => company[5])
# end


# # Add lines
# # Line.delete_all

# lines = [
#   ['FESCO China Direct Line', 'FCDL', "The route of «FESCO China Direct Line» is Voctochny — Vladivostok — Hong Kong — Ningbo — Shanghai — Vostochny including transshipment from ports of Xingang/Qingdao via Shanghai and transshipment from ports of Vietnam via Hong Kong when berth terms/rates are constant", Company.find_by_code('FESCO').id],
#   ['Korea Soviet Direct Line', 'KSDL', "The route of «KOREA Soviet Direct Line» Vostochny - Vladivostok - Busan - Ulsan - Vostochny - Vladivostok.", Company.find_by_code('FESCO').id],
#   ['Korea Sakhalin Line', 'FKSL', "The route of «FESCO Korea Sakhalin Line» is Korsakov — Kholmsk — Busan — Korsakov — Kholmsk.", Company.find_by_code('FESCO').id],
#   ['Japan Trans-Siberian Line', 'JTSL', "Direct regular sea container service on the route Vostochny — Vladivostok — Niigata — Toyamashinko — Moji — Kobe — Nagoya — Yokohama — Vostochny — Vladivostok.", Company.find_by_code('FESCO').id],
#   ['FESCO BALTORIENT Line', 'FBOL', "Shipping from South-East Asia ports to Russian towns and also to CIS countries through Saint-Petersburg port, Riga port, Klaipeda port.", Company.find_by_code('FESCO').id],
#   ["FESCO Baltorient Line (FBOL) Service Europe-Russian Far East and inland", 'FBOLER', "Shipments ex Europe to ports of Russian Far East and final rail transportation to Russian inland destinations", Company.find_by_code('FESCO').id],
#   ['FESCO ESF Service', 'FESF', "Shipping from European ports via port of Saint-Petersburg to Russian towns and CIS countries.", Company.find_by_code('FESCO').id],
#   ['Shanghai - Vladivostok', 'SASSV', "Shanghai - Vladivostok  Liner Service was launched in September 2010. Then, on the later of the line operation, SASCO vessels commenced to call on Vostochny port to discharge.  From September 2011 SASCO has been offering additional vessel call on Ningbo. Two sister ships are now operated on the line, mv Cape Charles and mv Cadiz, of deadweight 11400t and maximum cargo capacity 843 TEU each.", Company.find_by_code('SASCO').id],
#   ['Busan - Korsakov', 'SASBK', "Busan - Korsakov line was established in 1995 pursuant to relevant agreement between Russian and South Korean Governments. Duration of round voyage is about two weeks.", Company.find_by_code('SASCO').id],
#   ['Vladivostok-Vostochny-Korsakov', 'SASVK', "Since January, 2009 SASCO has resumed regular container service on Vladivostok - Korsakov line. Every 5 days SASCO vessels enter the port to deliver consumer goods to Sakhalin island and to carry locally produced goods to other country regions. Duration of round voyage is 10 days.", Company.find_by_code('SASCO').id],
#   ['Vladivostok-Vostochny-Magadan', 'SASVM',"Regular container service on Vladivostok-Magadan line has opened since November 2009.Vladivostok - Magadan line connects Vladivostok port on the north-western coast of the Japanese Sea with Magadan in Siberian North-East.", Company.find_by_code('SASCO').id],
#   ['Vladivostok - Vostochny - Petropavlovsk-Kamchatsky', 'SASVP',"In April 2011 SASCO new liner service from Vladivostok to Petropavlovsk-Kamchatsky was opened. Three SASCO liner vessels are operated on the line now.", Company.find_by_code('SASCO').id ],
#   ['Vanino-Magadan', 'SASVM', "Vanino-Magadan line delivers various containerized cargoes to Magadan. Vanino-Magadan line operates 'Pioner Moskvy' type vessels, as well as Ro-Lo vessels which intended both for horizontal and vertical loading (m/v Lyutoga). All the vessels are fully equipped for container carriages.", Company.find_by_code('SASCO').id],
#   ['Vanino-Kholmsk', 'SASVK', "Since the line Vanino - Kholmsk has been opened, railway wagons have been the dominant cargo carried by SASCO which is involved in implementation of multimodal agreements with Russian Railroad. Besides this, ferries engaged on the line carry more than 60 thousand passengers annually to maintain lasting communication for those who visit Sakhalin Island or leave for the Mainland.", Company.find_by_code('SASCO').id],
#   ['Korea Russia Service', 'PVS', "KOREA RUSSIA SERVICE (Pusan Vostochny SVC - PVS). Frequency - Weekly. Calling Port - Donghae - Busan - Vostochny", Company.find_by_code('SASCO').id],
#   ['Intra-Asia 8', 'IA8', "MCC Transport announces the revision of our Intra Asia 8 Service, IA-8. The revised IA-8 service will be a 35 day rotation, covering the ports of Singapore - Tanjung Pelepas – Jakarta – Surabaya – Davao – Shanghai – Busan – Vladivostok - Shanghai – Xiamen – Hong Kong – Yantian - Singapore. ", Company.find_by_code('MCC').id],
# ]

# lines.each do |line|
#   Line.find_or_create_by_name(
#                               :name        => line[0],
#                               :code        => line[1],
#                               :description => line[2],
#                               :company_id  => line[3])
# end

# # Add container type
# # Container.delete_all
# containers = [
#   ['20dc', "20 Dry Freight Container", "20′ container"],
#   ['40dc', "40 Dry Freight Container", ""],
#   ['40HC', "40 Dry High Cube Container", ""],
#   ['45HC', "45 Dry High Cube Container", ""],
#   ['40RC', "40 High Cube Ref Container", ""],
#   ['40RE', "40 foot Ref Container", ""],
#   ['20RE', "20 foot Ref Container", ""],
#   ['20OT', "20 Open Top Container ",""],
#   ['20HT', "20 Hard Top Container",""],
#   ['40OT', "40 Hard Top Container",""],
#   ['40HT', "20 Hard Top Container",""],
#   ['20FR', "20 Flatrack Container", ""],
#   ['40FR', "40 Flatrack Container", ""],
#   ['20TC', "20 foot Tank Container", ""],
# ]

# containers.each do |container|
#   Container.find_or_create_by_name(
#     :code        => container[0],
#     :name        => container[1],
#     :description => container[2])
# end

# #Add Ports
# def ports_csv (directory = "db/seed/ports/")
#   Dir.glob(directory + "*.csv") do |item|
#     csv_text = File.read(item)
#     CSV.parse(csv_text, :headers => true) do |row|
#       x = row.to_hash
#       y = { "country_id" => Country.find_by_code(item[-6, 2].upcase).id  , "description" => ''}
#       Port.find_or_create_by_name(x.merge!(y))
#     end
#   end
# end
# # Port.delete_all
# ports_csv


# # Add Fesco international tariffs
# def fesco_int_tariff_csv (directory = "db/seed/fescointtariff/")
#   Dir.glob(directory + "*.csv") do |item|
#     csv_text = File.read(item)
#     CSV.parse(csv_text, :headers => true) do |row|
#       tariff = row.to_hash
#       tariff["pol"] = Port.find_by_locode(tariff["pol"]).id
#       tariff["pod"] = Port.find_by_locode(tariff["pod"]).id
#       tariff["container_id"] = Container.find_by_code(tariff["container_id"]).id
#       tariff["line_id"] = Line.find_by_code(tariff["line_id"]).id
#       FescoIntTariff.find_or_create_by_pol_and_pod(tariff)
#     end
#   end
# end
# # FescoIntTariff.delete_all
# # fesco_int_tariff_csv


# Othc import from scv
def othc_csv (directory = "db/seed/othc/")
  Dir.glob(directory + "*.csv") do |item|
    csv_text = File.read(item)
    CSV.parse(csv_text, :headers => true) do |row|
      othc = row.to_hash
      othc["line_id"] = Line.find_by_code(othc["line_id"]).id
      ports = othc["port_ids"].split(",")
      ports.map! { |a| Port.find_by_locode(a).id }
      othc["port_ids"] = ports
      containers = othc["container_ids"].split(",")
      containers.map! { |a| Container.find_by_code(a).id }
      othc["container_ids"] = containers
      Othc.find_or_create_by_line_id_and_price_cents(othc)
    end
  end
end

# Othc.delete_all
othc_csv

# # Dthc import from csv
# def dthc_csv (directory = "db/seed/dthc/")
#   Dir.glob(directory + "*.csv") do |item|
#     csv_text = File.read(item)
#     CSV.parse(csv_text, :headers => true) do |row|
#       dthc = row.to_hash
#       dthc["line_id"] = Line.find_by_code(dthc["line_id"]).id
#       ports = dthc["port_ids"].split(",")
#       ports.map! { |a| Port.find_by_locode(a).id }
#       dthc["port_ids"] = ports
#       containers = dthc["container_ids"].split(",")
#       containers.map! { |a| Container.find_by_code(a).id }
#       dthc["container_ids"] = containers
#       Dthc.find_or_create_by_line_id_and_price_cents(dthc)
#     end
#   end
# end

# # Dthc.delete_all
# # dthc_csv


# # Fesco Intarnation Tariff import from csv
# def fesco_int_tariff_csv (directory = "db/seed/fescointtariff/")
#   Dir.glob(directory + "*.csv") do |item|
#     csv_text = File.read(item)
#     CSV.parse(csv_text, :headers => true) do |row|
#       tariff = row.to_hash
#       tariff["line_id"] = Line.find_by_code(tariff["line_id"]).id
#       containers = tariff["container_ids"].split(",")
#       containers.map! { |a| Container.find_by_code(a).id }
#       tariff["container_ids"] = containers
#       pols = tariff["pol_ids"].split(",")
#       pols.map! { |a| Port.find_by_locode(a).id }
#       tariff["pol_ids"] = pols
#       pods = tariff["pod_ids"].split(",")
#       pods.map! { |a| Port.find_by_locode(a).id }
#       tariff["pod_ids"] = pods
#       FescoIntTariff.find_or_create_by_line_id_and_freight_cents_and_currency(tariff)
#     end
#   end
# end

# FescoIntTariff.delete_all
# fesco_int_tariff_csv
