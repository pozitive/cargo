require 'test_helper'

class DthcsControllerTest < ActionController::TestCase
  setup do
    @dthc = dthcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dthcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dthc" do
    assert_difference('Dthc.count') do
      post :create, dthc: { container_id: @dthc.container_id, currency: @dthc.currency, line_id: @dthc.line_id, port_id: @dthc.port_id, price_cents: @dthc.price_cents }
    end

    assert_redirected_to dthc_path(assigns(:dthc))
  end

  test "should show dthc" do
    get :show, id: @dthc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dthc
    assert_response :success
  end

  test "should update dthc" do
    patch :update, id: @dthc, dthc: { container_id: @dthc.container_id, currency: @dthc.currency, line_id: @dthc.line_id, port_id: @dthc.port_id, price_cents: @dthc.price_cents }
    assert_redirected_to dthc_path(assigns(:dthc))
  end

  test "should destroy dthc" do
    assert_difference('Dthc.count', -1) do
      delete :destroy, id: @dthc
    end

    assert_redirected_to dthcs_path
  end
end
