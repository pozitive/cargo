require 'test_helper'

class PolFescoIntTariffsControllerTest < ActionController::TestCase
  setup do
    @pol_fesco_int_tariff = pol_fesco_int_tariffs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pol_fesco_int_tariffs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pol_fesco_int_tariff" do
    assert_difference('PolFescoIntTariff.count') do
      post :create, pol_fesco_int_tariff: { fesco_int_tariff_id: @pol_fesco_int_tariff.fesco_int_tariff_id, port_id: @pol_fesco_int_tariff.port_id }
    end

    assert_redirected_to pol_fesco_int_tariff_path(assigns(:pol_fesco_int_tariff))
  end

  test "should show pol_fesco_int_tariff" do
    get :show, id: @pol_fesco_int_tariff
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pol_fesco_int_tariff
    assert_response :success
  end

  test "should update pol_fesco_int_tariff" do
    patch :update, id: @pol_fesco_int_tariff, pol_fesco_int_tariff: { fesco_int_tariff_id: @pol_fesco_int_tariff.fesco_int_tariff_id, port_id: @pol_fesco_int_tariff.port_id }
    assert_redirected_to pol_fesco_int_tariff_path(assigns(:pol_fesco_int_tariff))
  end

  test "should destroy pol_fesco_int_tariff" do
    assert_difference('PolFescoIntTariff.count', -1) do
      delete :destroy, id: @pol_fesco_int_tariff
    end

    assert_redirected_to pol_fesco_int_tariffs_path
  end
end
