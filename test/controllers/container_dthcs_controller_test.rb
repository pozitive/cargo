require 'test_helper'

class ContainerDthcsControllerTest < ActionController::TestCase
  setup do
    @container_dthc = container_dthcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:container_dthcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create container_dthc" do
    assert_difference('ContainerDthc.count') do
      post :create, container_dthc: { container_id: @container_dthc.container_id, dthc_id: @container_dthc.dthc_id }
    end

    assert_redirected_to container_dthc_path(assigns(:container_dthc))
  end

  test "should show container_dthc" do
    get :show, id: @container_dthc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @container_dthc
    assert_response :success
  end

  test "should update container_dthc" do
    patch :update, id: @container_dthc, container_dthc: { container_id: @container_dthc.container_id, dthc_id: @container_dthc.dthc_id }
    assert_redirected_to container_dthc_path(assigns(:container_dthc))
  end

  test "should destroy container_dthc" do
    assert_difference('ContainerDthc.count', -1) do
      delete :destroy, id: @container_dthc
    end

    assert_redirected_to container_dthcs_path
  end
end
