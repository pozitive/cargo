require 'test_helper'

class OthcsControllerTest < ActionController::TestCase
  setup do
    @othc = othcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:othcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create othc" do
    assert_difference('Othc.count') do
      post :create, othc: { container_id: @othc.container_id, currency: @othc.currency, line_id: @othc.line_id, port_id: @othc.port_id, price_cents: @othc.price_cents }
    end

    assert_redirected_to othc_path(assigns(:othc))
  end

  test "should show othc" do
    get :show, id: @othc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @othc
    assert_response :success
  end

  test "should update othc" do
    patch :update, id: @othc, othc: { container_id: @othc.container_id, currency: @othc.currency, line_id: @othc.line_id, port_id: @othc.port_id, price_cents: @othc.price_cents }
    assert_redirected_to othc_path(assigns(:othc))
  end

  test "should destroy othc" do
    assert_difference('Othc.count', -1) do
      delete :destroy, id: @othc
    end

    assert_redirected_to othcs_path
  end
end
