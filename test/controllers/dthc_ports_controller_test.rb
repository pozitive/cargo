require 'test_helper'

class DthcPortsControllerTest < ActionController::TestCase
  setup do
    @dthc_port = dthc_ports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dthc_ports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dthc_port" do
    assert_difference('DthcPort.count') do
      post :create, dthc_port: { dthc_id: @dthc_port.dthc_id, port_id: @dthc_port.port_id }
    end

    assert_redirected_to dthc_port_path(assigns(:dthc_port))
  end

  test "should show dthc_port" do
    get :show, id: @dthc_port
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dthc_port
    assert_response :success
  end

  test "should update dthc_port" do
    patch :update, id: @dthc_port, dthc_port: { dthc_id: @dthc_port.dthc_id, port_id: @dthc_port.port_id }
    assert_redirected_to dthc_port_path(assigns(:dthc_port))
  end

  test "should destroy dthc_port" do
    assert_difference('DthcPort.count', -1) do
      delete :destroy, id: @dthc_port
    end

    assert_redirected_to dthc_ports_path
  end
end
