require 'test_helper'

class PodFescoIntTariffsControllerTest < ActionController::TestCase
  setup do
    @pod_fesco_int_tariff = pod_fesco_int_tariffs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pod_fesco_int_tariffs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pod_fesco_int_tariff" do
    assert_difference('PodFescoIntTariff.count') do
      post :create, pod_fesco_int_tariff: { fesco_int_tariff_id: @pod_fesco_int_tariff.fesco_int_tariff_id, port_id: @pod_fesco_int_tariff.port_id }
    end

    assert_redirected_to pod_fesco_int_tariff_path(assigns(:pod_fesco_int_tariff))
  end

  test "should show pod_fesco_int_tariff" do
    get :show, id: @pod_fesco_int_tariff
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pod_fesco_int_tariff
    assert_response :success
  end

  test "should update pod_fesco_int_tariff" do
    patch :update, id: @pod_fesco_int_tariff, pod_fesco_int_tariff: { fesco_int_tariff_id: @pod_fesco_int_tariff.fesco_int_tariff_id, port_id: @pod_fesco_int_tariff.port_id }
    assert_redirected_to pod_fesco_int_tariff_path(assigns(:pod_fesco_int_tariff))
  end

  test "should destroy pod_fesco_int_tariff" do
    assert_difference('PodFescoIntTariff.count', -1) do
      delete :destroy, id: @pod_fesco_int_tariff
    end

    assert_redirected_to pod_fesco_int_tariffs_path
  end
end
