require 'test_helper'

class ContainerOthcsControllerTest < ActionController::TestCase
  setup do
    @container_othc = container_othcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:container_othcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create container_othc" do
    assert_difference('ContainerOthc.count') do
      post :create, container_othc: { container_id: @container_othc.container_id, othc_id: @container_othc.othc_id }
    end

    assert_redirected_to container_othc_path(assigns(:container_othc))
  end

  test "should show container_othc" do
    get :show, id: @container_othc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @container_othc
    assert_response :success
  end

  test "should update container_othc" do
    patch :update, id: @container_othc, container_othc: { container_id: @container_othc.container_id, othc_id: @container_othc.othc_id }
    assert_redirected_to container_othc_path(assigns(:container_othc))
  end

  test "should destroy container_othc" do
    assert_difference('ContainerOthc.count', -1) do
      delete :destroy, id: @container_othc
    end

    assert_redirected_to container_othcs_path
  end
end
