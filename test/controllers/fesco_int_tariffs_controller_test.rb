require 'test_helper'

class FescoIntTariffsControllerTest < ActionController::TestCase
  setup do
    @fesco_int_tariff = fesco_int_tariffs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fesco_int_tariffs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fesco_int_tariff" do
    assert_difference('FescoIntTariff.count') do
      post :create, fesco_int_tariff: { baf_cents: @fesco_int_tariff.baf_cents, coc: @fesco_int_tariff.coc, container_id: @fesco_int_tariff.container_id, currency: @fesco_int_tariff.currency, freight_cents: @fesco_int_tariff.freight_cents, full: @fesco_int_tariff.full, line_id: @fesco_int_tariff.line_id }
    end

    assert_redirected_to fesco_int_tariff_path(assigns(:fesco_int_tariff))
  end

  test "should show fesco_int_tariff" do
    get :show, id: @fesco_int_tariff
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fesco_int_tariff
    assert_response :success
  end

  test "should update fesco_int_tariff" do
    patch :update, id: @fesco_int_tariff, fesco_int_tariff: { baf_cents: @fesco_int_tariff.baf_cents, coc: @fesco_int_tariff.coc, container_id: @fesco_int_tariff.container_id, currency: @fesco_int_tariff.currency, freight_cents: @fesco_int_tariff.freight_cents, full: @fesco_int_tariff.full, line_id: @fesco_int_tariff.line_id }
    assert_redirected_to fesco_int_tariff_path(assigns(:fesco_int_tariff))
  end

  test "should destroy fesco_int_tariff" do
    assert_difference('FescoIntTariff.count', -1) do
      delete :destroy, id: @fesco_int_tariff
    end

    assert_redirected_to fesco_int_tariffs_path
  end
end
