require 'test_helper'

class OthcPortsControllerTest < ActionController::TestCase
  setup do
    @othc_port = othc_ports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:othc_ports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create othc_port" do
    assert_difference('OthcPort.count') do
      post :create, othc_port: { othc_id: @othc_port.othc_id, port_id: @othc_port.port_id }
    end

    assert_redirected_to othc_port_path(assigns(:othc_port))
  end

  test "should show othc_port" do
    get :show, id: @othc_port
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @othc_port
    assert_response :success
  end

  test "should update othc_port" do
    patch :update, id: @othc_port, othc_port: { othc_id: @othc_port.othc_id, port_id: @othc_port.port_id }
    assert_redirected_to othc_port_path(assigns(:othc_port))
  end

  test "should destroy othc_port" do
    assert_difference('OthcPort.count', -1) do
      delete :destroy, id: @othc_port
    end

    assert_redirected_to othc_ports_path
  end
end
